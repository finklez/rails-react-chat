Rails.application.routes.draw do
  root 'chats#index'

  resources :chats, only: [:index]

  namespace :api do
    resources :chats, only: [:index, :create]
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
