module Api
  class ChatsController < ApiController
    before_action :set_chat, only: [:show, :update, :destroy]

    # GET /chats
    def index
      params = list_messages_params
      user_id = params[:user_id]
      time = Time.parse params[:updated_at]
      chats = Chat
                  .except_user(user_id)
                  .since(time)

      data = {
          updated_at: Time.now,
          users: Chat.users,
          messages: chats
      }

      render json: data
    end

    # POST /chats
    def create
      @chat = Chat.new(chat_params)

      res = if @chat.save
              {json: @chat, status: :created} #, location: @chat
            else
              {json: @chat.errors, status: :unprocessable_entity}
            end

      render res
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_chat
      @chat = Chat.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def chat_params
      params
          .require(:chat)
          .permit(:user_id, :text)
    end

    def list_messages_params
      params
          .permit(:user_id, :updated_at)
    end
  end
end
