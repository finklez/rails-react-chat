class ChatsController < ApplicationController
  before_action :set_chat, only: [:show, :update, :destroy]

  # GET /chats
  def index
    chats = Chat.all

    @data = {
        messages: chats.as_json,
        updated_at: Time.now,
        user_id: get_user,
        users: Chat.users
    }

    render file: 'public/chat'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_chat
    @chat = Chat.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def chat_params
    params.require(:chat)
        .permit(:user_id, :text)
  end

  def get_user
    User.uniqid
  end
end
