require 'securerandom'

class Chat < ApplicationRecord

  validates_length_of :user_id, is: 36
  validates_presence_of :text

  scope :since, ->(time) { where("created_at > ?", time) }
  scope :except_user, ->(user_id) { where.not(user_id: user_id) }


  def self.users
    distinct.pluck(:user_id)
  end

  private

  def uniqid
    SecureRandom.uuid
  end
end
