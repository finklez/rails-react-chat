require 'securerandom'

class User

  def self.uniqid
    SecureRandom.uuid
  end
end
