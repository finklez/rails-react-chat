import React from "react"
import PropTypes from "prop-types"
import { cleanUserId } from "../services/common";

import 'src/chat/chat-window'

const Message = props => (
  <li className="message">{props.msg.text}{' '}
    [{cleanUserId(props.msg.user_id)}]</li>
);

const ChatWindow = props => (
  <ul className="chat-window unstyled">
    {
      props.messages.map((msg, idx)=> (
        <Message key={idx} msg={msg}/>
      ))
    }
  </ul>
);

ChatWindow.propTypes = {
  greeting: PropTypes.string
};

export default ChatWindow
