import React from "react"
import PropTypes from "prop-types"
import { cleanUserId } from "../services/common";
import 'src/chat/users'

// const cleanUserId = userId => {
//   return userId.split('-')[0]
// };

const User = props => (
  <li className="user">{cleanUserId(props.user_id)}</li>
);

const Users = props => (
  <ul className="user-list unstyled">
    {
      props.users.map((user, idx)=> (
        <User key={idx} user_id={user}/>
      ))
    }
  </ul>
);

Users.propTypes = {
  greeting: PropTypes.string
};

export default Users
