import React from "react"
import PropTypes from "prop-types"
import Users from "./Users"
import MessageContainer from "../containers/message"
import ChatWindow from "./ChatWindow"
import { cleanUserId } from "../services/common";

import 'src/chat/chat'

const Hello = props => (
  <div className="hello">Hello{' '}
    <b>{cleanUserId(props.user_id)}</b>
    <span className="loading">{props.loading ? 'loading..' : ''}</span>
  </div>
);

const FETCH_TIMEOUT = 3e3;

class Chat extends React.Component {

  componentDidUpdate(prevProps) {
    const { data } = this.props.usersAndMessages;
    if(!data) return;

    const { data: prevData } = prevProps.usersAndMessages;
    const { user_id } = this.props.userDetails;

    const isUpdated = !prevData ||
      prevData.updated_at !== data.updated_at;

    if(isUpdated) {
      setTimeout(() =>
        this.props.fetchMessages(user_id, data.updated_at), FETCH_TIMEOUT);
    }
  }

  render () {
    const { data, isFetching } = this.props.usersAndMessages;
    const { user_id } = this.props.userDetails;
    // // wait for store update
    if(!data) {
      return null;
    }

    return (
      <div className={`main ${isFetching ? 'loading' : ''}`}>
        <Hello user_id={user_id}
               loading={isFetching}/>
        <div className="chat-wrapper">
          <div className="main-window">
            <ChatWindow messages={data.messages}/>
            <Users users={data.users}/>
          </div>
          <MessageContainer
            user_id={user_id}/>
        </div>
      </div>
    );
  }
}

// Chat.propTypes = {
//   greeting: PropTypes.string
// };

export default Chat
