import React from "react"
import PropTypes from "prop-types"

import 'src/chat/message'

class Message extends React.Component {
  state = {
    msg: ''
  };

  setMessage = e => {
    this.setState({msg: e.target.value})
  };

  sendMessage = () => {
    const { msg } = this.state;
    if(!msg.length) return;

    this.props.sendMessage(msg);
    this.setState({msg: ''});
  };

  sendOnEnterKey = e => {
    const isEnter = e.which === 13 ||
      e.keyCode === 13;
    if(!isEnter) return;

    this.sendMessage();
  };

  render() {
    return (
      <div className="message-wrapper">
        <input type="text"
               className="input"
               onChange={this.setMessage}
               value={this.state.msg}
               autoFocus={true}
               onKeyDown={this.sendOnEnterKey}/>
        <input type="button"
               className="btn submit"
               value='SEND'
               onClick={this.sendMessage}/>
      </div>
    );
  }
}

// Message.propTypes = {
//   greeting: PropTypes.string
// };

export default Message
