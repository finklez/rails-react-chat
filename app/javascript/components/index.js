import React from 'react'
// import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import rootReducer from '../reducers'

import ChatContainer from '../containers/chat'
import { receiveMessages, receiveUserDetails } from "../actions";

const store = createStore(
  rootReducer,
  applyMiddleware(
    thunkMiddleware, // lets us dispatch() functions
    // loggerMiddleware // neat middleware that logs actions
  )
);

class index extends React.Component {

  componentDidMount() {
    const { user_id, ...data } = this.props.data;
    store.dispatch(receiveMessages(data));
    store.dispatch(receiveUserDetails({ user_id }));
  }

  render() {
    return (
      <Provider store={store}>
        <ChatContainer/>
      </Provider>
    );
  }

}

export default index