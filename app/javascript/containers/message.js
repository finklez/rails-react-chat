import { connect } from 'react-redux'
import { receiveMessages,
  sendMessage } from '../actions'
import Message from '../components/Message'

const mapStateToProps = (state, _ownProps) => {
  return state;
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    sendMessage: (msg) => {
      dispatch(sendMessage(msg, ownProps.user_id));
      const messages = {
        messages: [{
          user_id: ownProps.user_id,
          text: msg
        }]
      };
      dispatch(receiveMessages(messages));
    }
  }
};

const MessageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Message);

export default MessageContainer