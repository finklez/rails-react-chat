import { connect } from 'react-redux'
import { fetchMessages } from '../actions'
import Chat from '../components/Chat'

const mapStateToProps = (state, _ownProps) => {
  return state;
};

const mapDispatchToProps = (dispatch, _ownProps) => {
  return {
    fetchMessages: (user_id, updated_at) => {
      dispatch(fetchMessages(user_id, updated_at))
    }
  }
};

const ChatContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Chat);

export default ChatContainer