import { combineReducers } from 'redux'
import { sendMessage,
  usersAndMessages,
  userDetails } from './chat'

const rootReducer = combineReducers({
  sendMessage,
  usersAndMessages,
  userDetails
});

export default rootReducer