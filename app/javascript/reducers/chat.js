import { REQUEST_MESSAGES,
  RECEIVE_MESSAGES,
  MESSAGE_SENT,
  RECEIVE_USER_DETAILS } from "../actions";

export const userDetails = (state = {}, action) => {
  switch (action.type) {
    case RECEIVE_USER_DETAILS:
      return action.user;
    default:
      return state;
  }
};

export const sendMessage = (state = '', action) => {
  switch (action.type) {
    case MESSAGE_SENT:
      return action.data;
    default:
      return state;
  }
};

export const usersAndMessages = (state = {isFetching: null}, action) => {
  switch (action.type) {
    case REQUEST_MESSAGES:
      return {
        ...state,
        isFetching: true
      };
    case RECEIVE_MESSAGES:
      const { messages: prevMessages } = state.data || { messages: [] };
      const { messages: newMessages } = action.json || { messages: [] };
      const messages = [...prevMessages, ...newMessages];

      return {
        isFetching: false,
        data: { ...state.data, ...action.json, messages }
      };
    default:
      return state;
  }
};

