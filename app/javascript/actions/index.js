import { postData } from "../services/common";

export const MESSAGE_SENT = 'MESSAGE_SENT';
function messageSent(data) {
  return {
    type: MESSAGE_SENT,
    data
  }
}

export const RECEIVE_USER_DETAILS = 'RECEIVE_USER_DETAILS';
export function receiveUserDetails(user) {
  return {
    type: RECEIVE_USER_DETAILS,
    user
  }
}

export const REQUEST_MESSAGES = 'REQUEST_MESSAGES';
function requestMessages() {
  return {
    type: REQUEST_MESSAGES
  }
}

export const RECEIVE_MESSAGES = 'RECEIVE_MESSAGES';
export function receiveMessages(json) {
  return {
    type: RECEIVE_MESSAGES,
    json
  }
}


export function fetchMessages(user_id, updated_at) {
  const getParams = {
    method: "GET",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/json",
      // "Authorization": token
    }
  };

  return function (dispatch) {
    dispatch(requestMessages());

    const url = `/api/chats?user_id=${user_id}&updated_at=${updated_at}`;
    return fetch(url, getParams)
      .then(
        response => response.json(),
        error => console.log('An error occurred.', error)
      )
      .then(json =>
        dispatch(receiveMessages(json))
    )
  }
}

export function sendMessage(text, user_id) {
  return function (dispatch) {
    postData('/api/chats', {chat: {text, user_id}})
      .then(data =>
        dispatch(messageSent(data)))
      .catch(error => console.error(error));
  }
}
