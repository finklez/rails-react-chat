# README

Install

Install Ruby
* make sure you have RVM installed

    
    rvm install ruby-2.4.3

Install bundler and Rails 

    gem install bundler
    bundle install
    
[Install Yarn and Node modules](https://yarnpkg.com/lang/en/docs/install/)

    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
    sudo apt-get update `&&` sudo apt-get install --no-install-recommends yarn
then

    Yarn install
    # npm install

Setup Database and tables
* make sure you have Postgres installed

Create a user `chat` with superuser permissions

    sudo -u postgres psql
    
    ``
    =# CREATE USER 
    =# ALTER USER chat WITH ENCRYPTED PASSWORD '123123';
    =# ALTER USER chat WITH SUPERUSER;
    ``
    
    
Setup DB

    rake db:setup
    rake db:migrate
    rake db:seed # optional
    
    
Troubleshooting

* Trouble installing `pg`
    https://stackoverflow.com/questions/6040583/cant-find-the-libpq-fe-h-header-when-trying-to-install-pg-gem
        
    It looks like in Ubuntu that header is part of the libpq-dev package (at least in the following Ubuntu versions: 11.04 (Natty Narwhal), 10.04 (Lucid Lynx), 11.10 (Oneiric Ocelot), 12.04 (Precise Pangolin) and 14.04 (Trusty Tahr)):
    
    ...
    /usr/include/postgresql/libpq-fe.h
    ...
    
    So try installing libpq-dev or its equivalent for your OS:

    For Ubuntu systems:
    
        sudo apt-get install libpq-dev
        
    On Red Hat Linux (RHEL) systems:
     
        yum install postgresql-devel