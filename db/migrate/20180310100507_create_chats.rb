class CreateChats < ActiveRecord::Migration[5.1]
  def change
    create_table :chats do |t|
      t.string :user_id
      t.text :text

      t.timestamps
    end

    add_index :chats, :created_at
  end
end
